package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func main() {
	router := gin.Default()

	router.GET("/", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"name": "Mateus Eric Hendaryanto",
			"bio":  "hapie",
		})
	})

	router.GET("/hello", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"title":    "Hello world",
			"subtitle": "belajar golang nich ngabzz",
		})
	})

	router.Run(":8888")
}
