package main

import (
	"fmt"
	"golang-api-gin/book"
	"golang-api-gin/handler"
	"log"

	"github.com/gin-gonic/gin"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func main() {
	dsn := "root:@tcp(127.0.0.1:3306)/golang-api-gin?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatal("Db connection error")
	}

	db.AutoMigrate(&book.Book{})

	//CRUD

	//create data

	// book := book.Book{}
	// book.Title = "Atomic habits"
	// book.Price = 190000
	// book.Discount = 15
	// book.Rating = 7
	// book.Description = "Ini adalah buku atomic habits"

	// err = db.Create(&book).Error
	// if err != nil {
	// 	fmt.Println("==========================")
	// 	fmt.Println("Error creating book record")
	// 	fmt.Println("==========================")
	// }

	// =========================================================

	// Get Data

	// var book book.Book

	// err = db.Debug().First(&book).Error
	// if err != nil {
	// 	fmt.Println("=====================")
	// 	fmt.Println("Error finding book")
	// 	fmt.Println("=====================")
	// }

	// fmt.Println("Title :", book.Title)
	// fmt.Printf("book object %v", book)

	// ===========================================================

	// FIND BOOK

	// var books []book.Book

	// err = db.Debug().Find(&books).Error
	// if err != nil {
	// 	fmt.Println("=====================")
	// 	fmt.Println("Error finding book")
	// 	fmt.Println("=====================")
	// }

	// for _, b := range books {

	// 	fmt.Println("Title :", b.Title)
	// 	fmt.Printf("book object %v", b)
	// }

	//============================================================

	// Find book with conditions

	// var books []book.Book

	// err = db.Debug().Where("rating = ?", "Man Tiger").Find(&books).Error
	// if err != nil {
	// 	fmt.Println("=====================")
	// 	fmt.Println("Error finding book")
	// 	fmt.Println("=====================")
	// }

	// for _, b := range books {

	// 	fmt.Println("Title :", b.Title)
	// 	fmt.Printf("book object %v", b)
	// }

	//=========================================================

	// UPDATE DATA

	var book book.Book

	err = db.Debug().Where("id = ?", 1).Find(&book).Error
	if err != nil {
		fmt.Println("=====================")
		fmt.Println("Error finding book")
		fmt.Println("=====================")
	}

	book.Title = "Man tiger (revisi)"
	err = db.Save(&book).Error
	if err != nil {
		fmt.Println("====================")
		fmt.Println("Error updating book")
		fmt.Println("====================")
	}
	//=======================================================

	// DELETE DATA

	err = db.Delete(&book).Error
	if err != nil {
		fmt.Println("==========================")
		fmt.Println("Error deleting book record")
		fmt.Println("==========================")
	}

	router := gin.Default()

	v1 := router.Group("/v1")

	v1.GET("/", handler.RootHandler)

	v1.GET("/hello", handler.HelloHandler)

	v1.GET("/books/:id/:title", handler.BooksHandler)

	v1.GET("/query", handler.QueryHandler)

	v1.POST("/books", handler.PostBooksHandler)

	router.Run(":8888")
}
